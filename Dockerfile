# based on oraclelinux image
FROM oraclelinux:latest

# install apache
RUN yum -y update
RUN yum -y install httpd httpd-tools

# install haproxy
RUN yum -y install haproxy
RUN yum clean all

# exposing 443 for https
EXPOSE 443

# sending breakout files (html, css, ...) to the container
ADD ./breakout /var/www/html

# puting the haproxy's configuration in the right place
COPY ./cert/haproxy.cfg /etc/haproxy/haproxy.cfg

# create a cert folder and puting the cert files on it
RUN mkdir -p /var/www/cert
ADD ./cert /var/www/cert

# copy the sh script to the container
COPY ./run-webserver.sh /root/run-webserver.sh

# delete httpd.pid to run the apache server without any issues
RUN rm -f /run/httpd/httpd.pid

# make the sh file executable
RUN chmod -v +x /root/run-webserver.sh

# point the ENTRYPOINT to the sh file
ENTRYPOINT ["/root/run-webserver.sh"]