#!/bin/bash

# run haproxy
haproxy -f /etc/haproxy/haproxy.cfg

# runing httpd on foreground to fix systemctl issue with centos : Failed to get D-Bus connection: Operation not permitted 
# for more details : https://forums.docker.com/t/any-simple-and-safe-way-to-start-services-on-centos7-systemd/5695/13 
# and https://linuxacademy.com/community/posts/show/topic/21629-docker-failed-to-get-dbus-connection-operation-not-permitted
/usr/sbin/httpd -DFOREGROUND